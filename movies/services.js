const {
    query
} = require('../data');

const getMovies = async() => {
    console.info('Retrieving all movies');
    const movies = await query("SELECT * FROM movies");
    return movies;
}

const getMovieById = async(id) => {
    console.info(`Retrieving movie with id ${id}`);

    const movies = await query("SELECT * FROM movies where id = $1", [id]);
    return movies[0];
}

const addMovie = async(title, genre, budget, release_date, imdb_score) => {
    console.info(`Adding new movie with title ${title}`);

    const movies = await query("INSERT INTO movies (title, genre, budget, release_date, imdb_score) VALUES ($1, $2, $3, $4, $5) RETURNING id", [title, genre, budget, release_date, imdb_score], (err, res) => {
        if (err) {
            console.log(`Error: ${err}`);
            result(err, null);
            return;
        }
        console.log('Added new movie');
    });

    return movies[0].id;
}

const updateMovieById = async(id, title, genre, budget, release_date, imdb_score) => {
    console.info(`Updating movie with id ${id}`);

    const movies = await query("UPDATE movies SET title = $2, genre = $3, budget = $4, release_date = $5, imdb_score = $6 WHERE id = $1", [id, title, genre, budget, release_date, imdb_score]);
    console.log(`Updated movie with id ${id}`);

    return movies[0];
}

const deleteMovieById = async(id) => {
    console.info(`Deleting movie with id ${id}`);

    const movies = await query("DELETE FROM movies WHERE id = $1", [id]);
    console.log(`Deleted movie with id ${id}`);
    return movies[0];
}

module.exports = {
    getMovies,
    getMovieById,
    addMovie,
    updateMovieById,
    deleteMovieById
}