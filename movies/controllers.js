const Router = require('express').Router();

const {
    getMovieById,
    getMovies,
    addMovie,
    updateMovieById,
    deleteMovieById
} = require('./services.js');

Router.get('/', async(req, res) => {
    const movies = await getMovies();
    res.json(movies);
});

Router.get('/:id', async(req, res) => {
    const {
        id
    } = req.params;

    const movie = await getMovieById(id);
    res.json(movie);
});

Router.post('/', async(req, res) => {
    const {
        title,
        genre,
        budget,
        release_date,
        imdb_score
    } = req.body;

    const id = await addMovie(title, genre, budget, release_date, imdb_score);
    res.json(id);
});

Router.put('/:id', async(req, res) => {
    const {
        id
    } = req.params;

    const {
        title,
        genre,
        budget,
        release_date,
        imdb_score
    } = req.body;

    const movie = await updateMovieById(id, title, genre, budget, release_date, imdb_score);
    res.json(movie);
});

Router.delete('/:id', async(req, res) => {
    const {
        id
    } = req.params;

    const movie = await deleteMovieById(id);
    res.json(movie);
});

module.exports = Router;