const Router = require('express').Router();

const MoviesController = require('./movies/controllers.js');

Router.use('/movies', MoviesController);

module.exports = Router;